﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WorkingWithData1.Startup))]
namespace WorkingWithData1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
