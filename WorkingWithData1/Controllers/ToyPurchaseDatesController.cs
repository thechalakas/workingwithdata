﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WorkingWithData1.Models;

namespace WorkingWithData1.Controllers
{
    public class ToyPurchaseDatesController : Controller
    {
        private CatContext db = new CatContext();

        // GET: ToyPurchaseDates
        public ActionResult Index()
        {
            var toyPurchaseDates = db.ToyPurchaseDates.Include(t => t.Cat);
            return View(toyPurchaseDates.ToList());
        }

        // GET: ToyPurchaseDates/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ToyPurchaseDate toyPurchaseDate = db.ToyPurchaseDates.Find(id);
            if (toyPurchaseDate == null)
            {
                return HttpNotFound();
            }
            return View(toyPurchaseDate);
        }

        // GET: ToyPurchaseDates/Create
        public ActionResult Create()
        {
            ViewBag.CatID = new SelectList(db.Cats, "ID", "CatName");
            return View();
        }

        // POST: ToyPurchaseDates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ToyPurchaseDateID,DateOfPurchase,ToyName,CatID")] ToyPurchaseDate toyPurchaseDate)
        {
            if (ModelState.IsValid)
            {
                db.ToyPurchaseDates.Add(toyPurchaseDate);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CatID = new SelectList(db.Cats, "ID", "CatName", toyPurchaseDate.CatID);
            return View(toyPurchaseDate);
        }

        // GET: ToyPurchaseDates/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ToyPurchaseDate toyPurchaseDate = db.ToyPurchaseDates.Find(id);
            if (toyPurchaseDate == null)
            {
                return HttpNotFound();
            }
            ViewBag.CatID = new SelectList(db.Cats, "ID", "CatName", toyPurchaseDate.CatID);
            return View(toyPurchaseDate);
        }

        // POST: ToyPurchaseDates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ToyPurchaseDateID,DateOfPurchase,ToyName,CatID")] ToyPurchaseDate toyPurchaseDate)
        {
            if (ModelState.IsValid)
            {
                db.Entry(toyPurchaseDate).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CatID = new SelectList(db.Cats, "ID", "CatName", toyPurchaseDate.CatID);
            return View(toyPurchaseDate);
        }

        // GET: ToyPurchaseDates/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ToyPurchaseDate toyPurchaseDate = db.ToyPurchaseDates.Find(id);
            if (toyPurchaseDate == null)
            {
                return HttpNotFound();
            }
            return View(toyPurchaseDate);
        }

        // POST: ToyPurchaseDates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ToyPurchaseDate toyPurchaseDate = db.ToyPurchaseDates.Find(id);
            db.ToyPurchaseDates.Remove(toyPurchaseDate);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
