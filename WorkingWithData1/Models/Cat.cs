﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WorkingWithData1.Models
{
    public class Cat
    {
        public int ID { get; set; }
        public string CatName { get; set; }

        public virtual ICollection<ToyPurchaseDate> ToyPurchaseDates { get; set; }

    }

    public class ToyPurchaseDate
    {
        public int ToyPurchaseDateID { get; set; }

        public string DateOfPurchase { get; set; }

        public string ToyName { get; set; }

        //this links up the below mentioned virtual cat property
        //this is the foreign key
        public int CatID { get; set; }

        //public virtual Student Student { get; set; }

        public virtual Cat Cat { get; set; }
    }

    public class CatContext : DbContext
    {
        public DbSet<Cat> Cats { get; set; }

        public System.Data.Entity.DbSet<WorkingWithData1.Models.ToyPurchaseDate> ToyPurchaseDates { get; set; }
    }
}